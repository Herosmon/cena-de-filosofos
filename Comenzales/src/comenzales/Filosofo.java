
package comenzales;  
import java.awt.Color;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextArea;




public class Filosofo implements Runnable{  
    
   int tiempo= 100;
   int procesos =10;
   
int id,res;  
Thread t;  
JButton filosofo;
JLabel derecho;
JLabel izquierdo;
JLabel resultado;
String proceso;
JTextArea textArea;
Filosofo(int id,JLabel izquierdo, JLabel derecho,JButton filosofo,JLabel resultado, JTextArea textArea){  
    this.id = id;  
    this.derecho = derecho;  
    this.izquierdo = izquierdo;  
    this.filosofo=filosofo;
    this.resultado=resultado;
    this.textArea=textArea;
    t = new Thread(this);
    t.start();  
    
}  
public void  run(){ 
    for(int i =0;i<procesos;i++){  
        synchronized(this.izquierdo){  
           synchronized(this.derecho){  
             comer();     
            } 
        }
        pensar();  
    } 
   
}  
void comer () {  
    derecho.setText("Ocupado");
    derecho.setForeground(Color.red);
  
    izquierdo.setText("Ocupado");
    izquierdo.setForeground(Color.red);
    
    filosofo.setText("Comiendo");
    filosofo.setBackground(Color.GREEN);

    res=Integer.parseInt(resultado.getText());
    res+=1;
    resultado.setText(String.valueOf(res));
    proceso= "Filosofo "+(id+1)+ " Esta comiendo y usa sus palillos.\n";
    textArea.append(proceso);
    try{  
        Thread.sleep(tiempo);  
    }catch(InterruptedException e){  
    }
    derecho.setText("Libre");
    derecho.setForeground(Color.black);
    
    izquierdo.setText("Libre");
    izquierdo.setForeground(Color.black);
    
    filosofo.setText("Pensando");   
    filosofo.setBackground(Color.DARK_GRAY);
    proceso="Filosofo "+(id+1)+ " deja de comer y se queda pensando, libera sus palillos.\n";  
    textArea.append(proceso);
}  
void pensar(){  
    derecho.setText("Libre");
    derecho.setForeground(Color.black);
    
    izquierdo.setText("Libre");
    izquierdo.setForeground(Color.black);
    
    filosofo.setText("Pensando");   
    filosofo.setBackground(Color.DARK_GRAY);

    try{  

        Thread.sleep(tiempo);  
    }catch(InterruptedException e){  
    }          
}  

}  